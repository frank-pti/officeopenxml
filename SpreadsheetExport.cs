using DocumentFormat.OpenXml.Model;
using DocumentFormat.OpenXml.Model.ChartDrawing;
using DocumentFormat.OpenXml.Model.SharedStringsTable;
using DocumentFormat.OpenXml.Model.Styles;
using DocumentFormat.OpenXml.Model.Workbooks;
using DocumentFormat.OpenXml.Model.WorksheetDrawings;
using DocumentFormat.OpenXml.Model.Worksheets;
using DocumentFormat.OpenXml.Parts;
using DocumentFormat.OpenXml.XmlHandling;
using Logging;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Xml;
using YAXLib;

namespace DocumentFormat.OpenXml
{
    public class SpreadsheetExport : ExportBase
    {
        private Package templatePackage;
        private List<string> sheetNames;
        private Dictionary<string, string> sheets;
        private Dictionary<string, Relationships> allParts;

        public SpreadsheetExport()
        {
            Initialize(
                new Dictionary<string, Relationships>(),
                new Workbook(),
                new SharedStrings(),
                new StyleSheet()
            );
            HeaderFooter = HeaderFooter.CreateDefault();
            Metadata = new Metadata("", "", "", "");
        }

        public SpreadsheetExport(FileInfo templateFile)
        {
            templatePackage = ZipPackage.Open(templateFile.FullName, FileMode.Open);
            try {
                Initialize(
                    LoadAllRelationships(templatePackage),
                    LoadWorkbook(templatePackage),
                    LoadSharedStrings(templatePackage),
                    LoadStyleSheet(templatePackage)
                );
            } catch (Exception e) {
                Console.WriteLine("Error loading template file: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        private void Initialize(
            Dictionary<string, Relationships> allParts,
            Workbook workbook, SharedStrings sharedStrings, StyleSheet styleSheet)
        {
            this.allParts = allParts;
            sheetNames = new List<string>();
            sheets = new Dictionary<string, string>();

            string relsPath = BuildRelsPath("/");
            Relationships rels = null;
            if (!allParts.ContainsKey(relsPath)) {
                rels = new Relationships();
                allParts.Add(relsPath, rels);
            } else {
                rels = allParts[relsPath];
            }
            Workbook = workbook;
            rels.Insert(workbook);

            foreach (Sheet sheet in workbook.Sheets) {
                sheetNames.Add(sheet.Name);
                sheets.Add(sheet.Name, sheet.RelId);
            }

            Relationships workbookRels = null;
            string workbookRelsPath = BuildRelsPath(Workbook.DocumentPath);
            if (!allParts.ContainsKey(workbookRelsPath)) {
                workbookRels = new Relationships();
                allParts.Add(workbookRelsPath, workbookRels);
            } else {
                workbookRels = allParts[workbookRelsPath];
            }

            workbookRels.RemoveAllByType(sharedStrings.Namespace);
            workbookRels.Insert(sharedStrings);
            if (sharedStrings != null) {
                SharedStrings = sharedStrings;
            } else {
                SharedStrings = new SharedStrings();
            }

            workbookRels.RemoveAllByType(styleSheet.Namespace);
            workbookRels.Insert(styleSheet);
            if (styleSheet != null) {
                StyleSheet = styleSheet;
            } else {
                StyleSheet = new StyleSheet();
            }

            workbookRels.RemoveAllByType(XmlNamespaces.OfficeOpenRelationshipCalcChain);
        }

        private SharedStrings LoadSharedStrings(Package package)
        {
            SharedStrings sharedStr = LoadFromXml<SharedStrings>(package, SharedStrings.DefaultDocumentPath);
            if (sharedStr == null) {
                sharedStr = new SharedStrings();
            }
            return sharedStr;

            //return LoadPart<SharedStrings>(package, SharedStrings.DefaultDocumentPath);
        }

        private StyleSheet LoadStyleSheet(Package package)
        {
            StyleSheet styleSheet = LoadFromXml<StyleSheet>(package, StyleSheet.DefaultDocumentPath);
            if (styleSheet == null) {
                return new StyleSheet();
            }
            return styleSheet;
        }

        private T LoadFromXml<T>(Package package, string document)
        {
            PackagePart part = LoadPart(package, document);
            XmlTextReader reader = new XmlTextReader(part.GetStream(FileMode.Open, FileAccess.Read));
            XmlDocument xml = new XmlDocument();
            xml.Load(reader);
            foreach (XmlNode node in xml.ChildNodes) {
                YAXSerializeAsAttribute[] attributes = (YAXSerializeAsAttribute[])
                    typeof(T).GetCustomAttributes(typeof(YAXSerializeAsAttribute), false);
                string localName = ((YAXSerializeAsAttribute)attributes[0]).SerializeAs;
                if (node.LocalName == localName) {
                    Logger.Log("Loading {0}", localName);
                    return Loader.Load<T>(node);
                }
            }
            return default(T);
        }

        private Workbook LoadWorkbook(Package package)
        {
            return LoadPart<Workbook>(package, Workbook.DefaultDocumentPath);
        }

        private Relationships LoadRelationships(Package package, string path)
        {
            return LoadPart<Relationships>(package, path);
        }

        private PackagePart LoadPart(Package package, string document)
        {
            return package.GetPart(new Uri(document, UriKind.Relative));
        }

        private T LoadPart<T>(Package package, string document)
        {
            PackagePart part = LoadPart(package, document);
            return XmlSerializable.DeserializeStreamXml<T>(part.GetStream(FileMode.Open, FileAccess.Read));
        }

        private Dictionary<string, Relationships> LoadAllRelationships(Package package)
        {
            Dictionary<string, Relationships> result = new Dictionary<string, Relationships>();
            foreach (PackagePart part in package.GetParts()) {
                if (part.ContentType == ContentTypes.RelationshipsXml) {
                    Relationships relationships = LoadPart<Relationships>(package, part.Uri.OriginalString);
                    result.Add(part.Uri.OriginalString, relationships);
                }
            }
            return result;
        }

        SharedStrings sharedStrings;
        public SharedStrings SharedStrings
        {
            get
            {
                if (sharedStrings == null) {
                    sharedStrings = new SharedStrings();
                    Logger.Warning("Creating shared strings in get method of SharedString property in SpreadsheetExport class!");
                }
                return sharedStrings;
            }
            private set
            {
                sharedStrings = value;
            }
        }

        public StyleSheet StyleSheet
        {
            get;
            private set;
        }

        public Workbook Workbook
        {
            get;
            private set;
        }

        public HeaderFooter HeaderFooter
        {
            get;
            set;
        }

        public void InsertWorksheet(Worksheet worksheet, string caption)
        {
            if (HeaderFooter != null) {
                worksheet.HeaderFooter = HeaderFooter;
            }
            Relationships workbookRels = allParts[BuildRelsPath(Workbook.DocumentPath)];
            bool existingSheet = sheetNames.Contains(caption);
            if (existingSheet) {
                worksheet.RelId = sheets[caption];
            }
            Relationship.Create(workbookRels, worksheet);
            if (!existingSheet) {
                sheets.Add(caption, worksheet.RelId);
                sheetNames.Add(caption);
            }
        }

        public void InsertChart(
            Worksheet worksheet, WorksheetDrawingFactory drawingFactory, ChartSpace chart, string identifier)
        {
            // drawing
            string drawingFileName = WorksheetDrawing.BuildDocumentPathFromFilename(
                String.Format("drawing_{0}.xml", identifier));
            Relationships drawingRelationships = new Relationships();

            string drawingRelsPath = BuildRelsPath(worksheet.DocumentPath);
            if (allParts.ContainsKey(drawingRelsPath)) {
                allParts[drawingRelsPath] = drawingRelationships;
            } else {
                allParts.Add(drawingRelsPath, drawingRelationships);
            }

            // chart
            Relationships chartRelationships = new Relationships();
            Relationship chartRel = chartRelationships.Insert(chart);
            string chartRelsPath = BuildRelsPath(drawingFileName);
            if (allParts.ContainsKey(chartRelsPath)) {
                allParts[chartRelsPath] = chartRelationships;
            } else {
                allParts.Add(chartRelsPath, chartRelationships);
            }

            WorksheetDrawing drawing = drawingFactory.ProduceWorksheetDrawing(chartRel.Id, drawingFileName);
            Relationship drawingRel = drawingRelationships.Insert(drawing);
            worksheet.Drawing = new Drawing() {
                RelId = drawingRel.Id
            };
        }

        public void InsertCustom(IPart part)
        {
            Relationships mainRels = allParts[BuildRelsPath("/")];
            mainRels.RemoveByTarget(part.DocumentPath);
            mainRels.Insert(part);
        }

        public void Close()
        {
            if (templatePackage != null) {
                templatePackage.Close();
            }
        }

        #region implemented abstract members of DocumentFormat.OpenXml.ExportBase
        protected override void FillZipPackage(Package package)
        {
            UpdateWorkbookSheets();

            Relationships workbookRelationships = allParts[BuildRelsPath(Workbook.DocumentPath)];
            workbookRelationships.RemoveAllByType(XmlNamespaces.OfficeOpenRelationshipCalcChain);

            Relationships mainRels = allParts[BuildRelsPath("/")];
            if (Metadata != null) {
                mainRels.RemoveAllByType(Metadata.CoreProperties.Namespace);
                mainRels.Insert(Metadata.CoreProperties);
                mainRels.RemoveAllByType(Metadata.AppProperties.Namespace);
                mainRels.Insert(Metadata.AppProperties);
            }
            mainRels.RemoveAllByType(Workbook.Namespace);
            mainRels.Insert(Workbook);

            foreach (string rel in allParts.Keys) {
                Relationships relationships = allParts[rel];

                SerializePartToPackage(package, relationships, rel, ContentTypes.RelationshipsXml);

                foreach (Relationship relationship in relationships.Children) {
                    string target = relationship.Target;
                    if (target == null) {
                        Console.WriteLine("target: {0}, {1}", target, relationship.Type);
                    }
                    if (!target.StartsWith("/")) {
                        target = String.Format("../{0}", relationship.Target);
                    }
                    string path = BuildAbsolutePath(rel, target);
                    if (relationship.Part == null) {
                        PackagePart part = LoadPart(templatePackage, path);
                        if (part.ContentType == ContentTypes.WorksheetXml) {
                            relationship.Part = new WorksheetCopyPart(
                                path, relationship.Id, part.GetStream());
                        } else {
                            relationship.Part = new CopyPart(
                                part.ContentType, path, relationship.Id, relationship.Type, part.GetStream());
                        }
                    }
                    SerializePartToPackage(package, relationship.Part, path, relationship.Part.ContentType);
                }
            }
        }
        #endregion

        private string BuildAbsolutePath(string source, string destination)
        {
            if (!source.StartsWith("/")) {
                throw new ArgumentException("source path must be absolute", "source");
            }
            if (destination.StartsWith("/")) {
                return destination;
            }
            int lastSlashIndex = source.LastIndexOf("/");
            source = source.Substring(0, lastSlashIndex);
            while (destination.StartsWith("../")) {
                lastSlashIndex = source.LastIndexOf("/");
                if (lastSlashIndex < 0) {
                    throw new ArgumentException("Destination path is not relative to source");
                }
                source = source.Substring(0, lastSlashIndex);
                destination = destination.Substring("../".Length);
            }
            return String.Format("{0}/{1}", source, destination);
        }

        private string BuildRelsPath(string targetPath)
        {
            if (!targetPath.StartsWith("/")) {
                throw new ArgumentException("Target path does not start with the '/' character", "targetPath");
            }
            int slashIndex = targetPath.LastIndexOf('/');
            string directory = targetPath.Substring(0, slashIndex + 1);
            string fileName = targetPath.Substring(slashIndex + 1);
            return String.Format("{0}_rels/{1}.rels", directory, fileName);
        }

        private void UpdateWorkbookSheets()
        {
            Workbook.Sheets.Clear();
            foreach (string name in sheetNames) {
                Workbook.AppendSheet(name, sheets[name]);
            }
        }

        private void SerializePartToPackage(Package package, IStreamSerializable part, string path, string contentType)
        {
            Uri uri = new Uri(path, UriKind.Relative);
            PackagePart packagePart = package.CreatePart(uri, contentType);
            Stream stream = packagePart.GetStream(FileMode.Create, FileAccess.Write);
            part.Serialize(stream);
            stream.Close();
        }
    }
}

