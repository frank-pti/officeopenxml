/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace DocumentFormat.OpenXml.Enums
{
    public enum CharsetType: int
    {
        DefaultCharset = 1,
        SymbolCharset = 2,
        MacCharset = 77,
        ShiftjisCharset = 128,
        HangulCharset = 129,
        JohabCharset = 130,
        Gb2312Charset = 134,
        ChineseBig5Charset = 136,
        GreekCharset = 161,
        TurkishCharset = 162,
        VietnameseCharset = 163,
        HebrewCharset = 177,
        ArabicCharset = 178,
        BalticCharset = 186,
        RussianCharset = 204,
        ThaiCharset = 222,
        EasteuropeCharset = 238,
        OemCharset = 255
    }
}

