/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Enums
{
    public enum TableStyleType
    {
        [YAXEnumAttribute("blankRow")]
        BlanRow,

        [YAXEnumAttribute("firstColumn")]
        FirstColumn,

        [YAXEnumAttribute("firstColumnStripe")]
        FirstColumnStripe,

        [YAXEnumAttribute("firstColumnSubheading")]
        FirstColumnSubheading,

        [YAXEnumAttribute("firstHeaderRowStyle")]
        FirstHeaderRow,

        [YAXEnumAttribute("firstRowStripe")]
        FirstRowStripe,

        [YAXEnumAttribute("firstRowSubheading")]
        FirstRowSubheading,

        [YAXEnumAttribute("firstSubtotalColumn")]
        FirstSubtotalColumn,

        [YAXEnumAttribute("firstSubtotalRow")]
        FirstSubtotalRow,

        [YAXEnumAttribute("firstTotalRow")]
        FirstTotalRow,

        [YAXEnumAttribute("firstTotalCell")]
        FirstTotalCell,

        [YAXEnumAttribute("headerRow")]
        HeaderRow,

        [YAXEnumAttribute("lastColumn")]
        LastColumn,

        [YAXEnumAttribute("lastHeaderCell")]
        LastHeaderCell,

        [YAXEnumAttribute("lastTotalCell")]
        LastTotalCell,

        [YAXEnumAttribute("pageFieldLabels")]
        PageFieldLabels,

        [YAXEnumAttribute("pageFieldValues")]
        PageFieldValues,

        [YAXEnumAttribute("secondColumnStripe")]
        SecondColumnStripe,

        [YAXEnumAttribute("secondColumnSubheading")]
        SecondColumnSubHeading,

        [YAXEnumAttribute("secondRowStripe")]
        SecondRowStripe,

        [YAXEnumAttribute("secondRowSubheading")]
        SecondRowSubheading,

        [YAXEnumAttribute("secondSubtotalColumn")]
        SecondSubtotalColumn,

        [YAXEnumAttribute("secondSubtotalRow")]
        SecondSubtotalRow,

        [YAXEnumAttribute("thirdClumnSubheading")]
        ThirdColumnSubheading,

        [YAXEnumAttribute("thirdRowSubheading")]
        ThirdRowSubheading,

        [YAXEnumAttribute("thirdSubtotalColumn")]
        ThirdSubtotalColumn,

        [YAXEnumAttribute("thirdSubtotalRow")]
        ThirdSubtotalRow,

        [YAXEnumAttribute("totalRow")]
        TotalRow,

        [YAXEnumAttribute("wholeTable")]
        WholeTable
    }
}

