/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using DocumentFormat.OpenXml.Model;

namespace DocumentFormat.OpenXml.Enums
{
    public enum PaperSize: int
    {
        [PaperSizeAttribute(297, 420, Unit.Millimeter)]
        A3 = 8,

        [PaperSizeAttribute(210, 297, Unit.Millimeter)]
        A4 = 9,

        [PaperSizeAttribute(210, 297, Unit.Millimeter)]
        A4small = 10,

        [PaperSizeAttribute(148, 210, Unit.Millimeter)]
        A5 = 11,

        [PaperSizeAttribute(250, 353, Unit.Millimeter)]
        B4 = 12,

        [PaperSizeAttribute(176, 250, Unit.Millimeter)]
        B5 = 13,

        [PaperSizeAttribute(236, 322, Unit.Millimeter)]
        A4extra = 53,

        [PaperSizeAttribute(210, 297, Unit.Millimeter)]
        A4transverse = 55,

        [PaperSizeAttribute(210, 330, Unit.Millimeter)]
        A4plus = 60,

        [PaperSizeAttribute(322, 445, Unit.Millimeter)]
        A3extra = 63,

        [PaperSizeAttribute(174, 235, Unit.Millimeter)]
        A5extra = 64,

        [PaperSizeAttribute(420, 594, Unit.Millimeter)]
        A2 = 66,

        [PaperSizeAttribute(297, 420, Unit.Millimeter)]
        A3transverse = 67,

        [PaperSizeAttribute(322, 445, Unit.Millimeter)]
        A3extraTransverse = 68,

        [PaperSizeAttribute(105, 148, Unit.Millimeter)]
        A6 = 70
    }
}

