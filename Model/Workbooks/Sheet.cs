/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Workbooks
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("sheet")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Sheet
    {
        private const int MaximumSheetNameLength = 31;

        private string name;

        public Sheet()
        {
        }

        private Sheet(string name, int sheetId, string relId)
        {
            Name = name;
            SheetId = sheetId;
            RelId = relId;
        }

        public static Sheet Create(Workbook parent, string name, string relId)
        {
            Sheet sheet = new Sheet(name, parent.NextId(), relId);
            parent.Sheets.Add(sheet);
            return sheet;
        }

        [YAXSerializableField]
        [YAXSerializeAs ("name")]
        [YAXAttributeForClass]
        public string Name {
            get {
                return name;
            }
            set {
                if (value != null && value.Length > MaximumSheetNameLength) {
                    throw new ArgumentException(
                        String.Format("Maximum length of sheet name is 31 characters due to MS Excel limitations. The string was: '{0}' (length: {1})", value, value.Length), 
                        "value");
                }
                name = value;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("sheetId")]
        [YAXAttributeForClass]
        public int SheetId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
        [YAXNamespace("r", XmlNamespaces.OfficeOpenDocumentRelationships)]
        public string RelId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("state")]
        [YAXAttributeForClass]
        public string State {
            get {
                return "visible";
            }
        }
    }
}

