/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Drawing
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("lstStyles")]
    [YAXNamespace("a", XmlNamespaces.OfficeOpenDrawing)]
	public class TextRunProperties
	{
        private const int SizeMinimum = 100;
        private const int SizeMaximum = 400000;
        private const int DefaultSize = 1400;

        private int textFontSize;

        public TextRunProperties ()
        {
            TextFontSize = DefaultSize;
        }

        [YAXSerializableField]
        [YAXSerializeAs("sz")]
        [YAXAttributeForClass]
        public int TextFontSize {
            get {
                return textFontSize;
            }
            set {
                if (value < SizeMinimum || value > SizeMaximum) {
                    throw new ArgumentOutOfRangeException(
                        String.Format("Text font size has a minimum value of greater than or equal to {0} and a maximum value fo less or equal to {1}", SizeMinimum, SizeMaximum));
                }
                textFontSize = value;
            }
        }

        public double TextFontSizeInPoints {
            get {
                return TextFontSize / 100;
            }
            set {
                TextFontSize = (int)Math.Round(value * 100);
            }
        }
	}
}

