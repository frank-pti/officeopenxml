/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO.Packaging;

namespace DocumentFormat.OpenXml.Model
{
    public abstract class PackageXmlSerializable: XmlSerializable
    {
        public PackageXmlSerializable ()
        {
        }

        public void ExportToPackage(Package package, string documentPath)
        {
            Uri partUriDocument = PackUriHelper.CreatePartUri(
                new Uri(documentPath, UriKind.Relative));
            PackagePart part = package.CreatePart(partUriDocument, ContentType);
            SerializeStreamXml(part.GetStream());
        }

        public abstract string ContentType {
            get;
        }
    }
}

