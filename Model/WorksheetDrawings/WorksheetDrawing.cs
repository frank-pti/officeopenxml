/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.WorksheetDrawings
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("wsDr")]
    [YAXNamespace("xdr", XmlNamespaces.OfficeOpenDrawingSpreadsheet)]
    public class WorksheetDrawing: PackageXmlSerializable, IPart
    {
        [Obsolete("for yaxlib deserialization only")]
        public WorksheetDrawing ()
        {
        }

        public WorksheetDrawing(string documentPath)
        {
            DocumentPath = documentPath;
        }

        [YAXSerializableField]
        [YAXSerializeAs("twoCellAnchor")]
        public TwoCellAnchor Anchor {
            get;
            set;
        }

        #region implemented abstract members of ProbeNet.Mini.Export.OfficeOpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.DrawingXml;
            }
        }
        #endregion        

        #region IPart implementation
        public void Serialize (System.IO.Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get;
            private set;
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipDrawing;
            }
        }
        #endregion

        public static string BuildDocumentPathFromFilename(string filename)
        {
            if (!filename.EndsWith(".xml")) {
                filename += ".xml";
            }
            return String.Format("/xl/drawings/{0}", filename);
        }
    }
}

