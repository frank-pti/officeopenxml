/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.WorksheetDrawings
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("nvGraphicFramePr")]
    [YAXNamespace("xdr", XmlNamespaces.OfficeOpenDrawingSpreadsheet)]
	public class NonVisualDrawingPropertiesForGraphicFrame
	{
        [YAXSerializableField]
        [YAXSerializeAs("cNvPr")]
        public NonVisualDrawingProperties NonVisualDrawingProperties {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("cNvGraphicFramePr")]
        public NonVisualGraphicFrameDrawingProperties NonVisualGraphicFrameDrawingProperties {
            get;
            set;
        }
	}
}

