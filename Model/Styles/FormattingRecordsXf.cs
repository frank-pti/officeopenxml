/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("xf")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class FormattingRecordsXf
    {
        public FormattingRecordsXf():
            this(new XfApplyTypes[0])
        {
        }

        public FormattingRecordsXf(params XfApplyTypes[] applyTypes)
        {
            foreach (XfApplyTypes types in applyTypes) {
                ApplyAlignment = (types & XfApplyTypes.ApplyAlignment) > 0;
                ApplyBorder = (types & XfApplyTypes.ApplyBorder) > 0;
                ApplyFill = (types & XfApplyTypes.ApplyFill) > 0;
                ApplyFont = (types & XfApplyTypes.ApplyFont) > 0;
                ApplyNumberFormat = (types & XfApplyTypes.ApplyNumberFormat) > 0;
                ApplyProtection = (types & XfApplyTypes.ApplyProtection) > 0;
            }
        }

        protected FormattingRecordsXf(FormattingRecordsXf xf)
        {
            ApplyAlignment = xf.ApplyAlignment;
            Alignment = xf.Alignment;
            Protection = xf.Protection;
            ApplyBorder = xf.ApplyBorder;
            BorderId = xf.BorderId;
            ApplyFill = xf.ApplyFill;
            FillId = xf.FillId;
            ApplyFont = xf.ApplyFont;
            FontId = xf.FontId;
            ApplyNumberFormat = xf.ApplyNumberFormat;
            NumberFormatId = xf.NumberFormatId;
            ApplyProtection = xf.ApplyProtection;
            PivotButton = xf.PivotButton;
            QuotePrefix = xf.QuotePrefix;
        }

        //TODO: implement child elements:
        //  extLst
        //  protection

        [YAXSerializableField]
        [YAXSerializeAs("alignment")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Alignment Alignment {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("protection")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Protection Protection {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyAlignment")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyAlignment {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyBorder")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyBorder {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyFill")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyFill {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyFont")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyFont {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyNumberFormat")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyNumberFormat {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("applyProtection")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> ApplyProtection {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("borderId")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> BorderId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("fillId")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> FillId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("fontId")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> FontId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("numFmtId")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> NumberFormatId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("pivotButton")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public Nullable<bool> PivotButton {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("quotePrefix")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public Nullable<bool> QuotePrefix {
            get;
            set;
        }

        public static bool operator == (FormattingRecordsXf a, FormattingRecordsXf b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }

            bool equalAlignment = AreBothTrue(a.ApplyAlignment, b.ApplyAlignment) ?
                a.Alignment == b.Alignment : AreEqual (a.ApplyAlignment, b.ApplyAlignment);
            bool equalProtection = AreBothTrue(a.ApplyProtection, b.ApplyProtection) ?
                a.Protection == b.Protection : AreEqual (a.ApplyProtection, b.ApplyProtection);
            bool equalBorder = AreBothTrue(a.ApplyBorder, b.ApplyBorder) ?
                a.BorderId == b.BorderId : AreEqual (a.BorderId, b.BorderId);
            bool equalFill = AreBothTrue(a.ApplyFill, b.ApplyFill) ?
                a.FillId == b.FillId : AreEqual(a.ApplyFill, b.ApplyFill);
            bool equalFont = AreBothTrue(a.ApplyFont, b.ApplyFont) ?
                a.FontId == b.FontId : AreEqual(a.ApplyFont, b.ApplyFont);
            bool equalNumberFormat = AreBothTrue(a.ApplyNumberFormat, b.ApplyNumberFormat) ?
                a.NumberFormatId == b.NumberFormatId : AreEqual(a.ApplyNumberFormat, b.ApplyNumberFormat);
            return equalAlignment && equalProtection && equalBorder && equalFill && equalFont && equalNumberFormat &&
                    a.PivotButton == b.PivotButton &&
                    a.QuotePrefix == b.QuotePrefix;
        }

        private static bool AreBothTrue (Nullable<bool> a, Nullable<bool> b)
        {
            return AreEqual (a, b) && a != null && a.Value;
        }

        private static bool AreEqual<T> (Nullable<T> a, Nullable<T> b)
            where T: struct
        {
            if (a == null && b == null) {
                return true;
            }
            if (a == null || b == null) {
                return false;
            }
            return a.Value.Equals(b.Value);
        }

        public static bool operator != (FormattingRecordsXf a, FormattingRecordsXf b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[FormattingRecordsXf: Alignment={0}, Protection={1}, ApplyAlignment={2}, ApplyBorder={3}, ApplyFill={4}, ApplyFont={5}, ApplyNumberFormat={6}, ApplyProtection={7}, BorderId={8}, FillId={9}, FontId={10}, NumberFormatId={11}, PivotButton={12}, QuotePrefix={13}]", Alignment, Protection, ApplyAlignment, ApplyBorder, ApplyFill, ApplyFont, ApplyNumberFormat, ApplyProtection, BorderId, FillId, FontId, NumberFormatId, PivotButton, QuotePrefix);
        }

        public override bool Equals (object obj)
        {
            if (obj is FormattingRecordsXf) {
                return ((obj as FormattingRecordsXf) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return ApplyAlignment.GetHashCode() ^ ApplyBorder.GetHashCode() ^ ApplyFill.GetHashCode() ^
                    ApplyFont.GetHashCode() ^ ApplyNumberFormat.GetHashCode() ^ ApplyProtection.GetHashCode() ^
                    Protection.GetHashCode() ^ Alignment.GetHashCode() ^ BorderId.GetHashCode() ^
                    FillId.GetHashCode() ^ FontId.GetHashCode() ^ NumberFormatId.GetHashCode() ^
                    PivotButton.GetHashCode() ^ QuotePrefix.GetHashCode();
        }

        public static FormattingRecordsXf LoadFromXml (XmlNode node)
        {
            /*
            FormattingRecordsXf xf = new FormattingRecordsXf(new XfApplyTypes[0]);
            foreach (XmlAttribute attribute in node.Attributes) {
                switch (attribute.LocalName) {
                case "applyAlignment":
                    xf.ApplyAlignment = attribute.Value == "1";
                    break;
                case "applyBorder":
                    xf.ApplyBorder = attribute.Value == "1";
                    break;
                case "applyFill":
                    xf.ApplyFill = attribute.Value == "1";
                    break;
                case "applyFont":
                    xf.ApplyFont = attribute.Value == "1";
                    break;
                case "applyNumberFormat":
                    xf.ApplyNumberFormat = attribute.Value == "1";
                    break;
                case "applyProtection":
                    xf.ApplyProtection = attribute.Value == "1";
                    break;
                case "borderId":
                    xf.BorderId = uint.Parse(attribute.Value);
                    break;
                case "fillId":
                    xf.FillId = uint.Parse(attribute.Value);
                    break;
                case "fontId":
                    xf.FontId = uint.Parse(attribute.Value);
                    break;
                case "numFmtId":
                    xf.NumberFormatId = uint.Parse(attribute.Value);
                    break;
                case "pivotButton":
                    xf.PivotButton = attribute.Value == "1";
                    break;
                case "quotePrefix":
                    xf.QuotePrefix = attribute.Value == "1";
                    break;
                default:
                    Logging.Logger.Log("FormattingRecordsXf xf has unknwon attribute: {0}", attribute.LocalName);
                    break;
                }
            }
            foreach (XmlNode child in node.ChildNodes) {
                switch (child.LocalName) {
                case "alignment":
                    xf.Alignment = Alignment.LoadFromXml(child);
                    break;
                case "protection":
                    xf.Protection = Protection.LoadFromXml(child);
                    break;
                default:
                    Logging.Logger.Log("FormattingRecords xf has unknwon element: {0}", child.LocalName);
                    break;
                }
            }
            */
            FormattingRecordsXf xf = Loader.Load<FormattingRecordsXf>(node);
            return xf;
        }
    }
}

