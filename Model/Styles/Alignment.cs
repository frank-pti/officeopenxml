/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using DocumentFormat.OpenXml.Enums;
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("alignment")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Alignment
    {
        private uint textRotation;

        public Alignment ()
        {
            HorizontalAlignment = HorizontalAlignmentType.General;
            Indent = 0;
            JustifyLastLine = false;
            ReadingOrderType = ReadingOrderType.ContextDependent;
            RelativeIndent = 0;
            ShrinkToFit = false;
            TextRotation = 0;
            VerticalAlignment = VerticalAlignmentType.AlignedToBottom;
            WrapText = false;
        }

        [YAXSerializableField]
        [YAXSerializeAs("horizontal")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Warning)]
        public HorizontalAlignmentType HorizontalAlignment {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("indent")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint Indent {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("justifyLastLine")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool JustifyLastLine {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("readingOrder")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint ReadingOrder {
            get;
            set;
        }

        public ReadingOrderType ReadingOrderType {
            get {
                return (ReadingOrderType) ReadingOrder;
            }
            set {
                ReadingOrder = Convert.ToUInt32(value);
            }
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("realtiveIndent")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public int RelativeIndent {
            get;
            set;
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("shrinkToFit")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool ShrinkToFit {
            get;
            set;
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("textRotation")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint TextRotation {
            get {
                return textRotation;
            }
            set {
                if (textRotation > 180) {
                    throw new ArgumentOutOfRangeException("Text rotation is limited to range 0 to 180 degrees.");
                }
                textRotation = value;
            }
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("vertical")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public VerticalAlignmentType VerticalAlignment {
            get;
            set;
        }

        
        [YAXSerializableField]
        [YAXSerializeAs("wrapText")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool WrapText {
            get;
            set;
        }

        public static bool operator == (Alignment a, Alignment b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.HorizontalAlignment == b.HorizontalAlignment && a.Indent == b.Indent &&
                    a.JustifyLastLine == b.JustifyLastLine && a.ReadingOrder == b.ReadingOrder &&
                    a.RelativeIndent == b.RelativeIndent && a.ShrinkToFit == b.ShrinkToFit &&
                    a.TextRotation == b.TextRotation && a.VerticalAlignment == b.VerticalAlignment &&
                    a.WrapText == b.WrapText;
        }

        public static bool operator != (Alignment a, Alignment b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Alignment: HorizontalAlignment={0}, Indent={1}, JustifyLastLine={2}, ReadingOrder={3}, ReadingOrderType={4}, RelativeIndent={5}, ShrinkToFit={6}, TextRotation={7}, VerticalAlignment={8}, WrapText={9}]", HorizontalAlignment, Indent, JustifyLastLine, ReadingOrder, ReadingOrderType, RelativeIndent, ShrinkToFit, TextRotation, VerticalAlignment, WrapText);
        }

        public override bool Equals (object obj)
        {
            if (obj is Alignment) {
                return ((obj as Alignment) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return HorizontalAlignment.GetHashCode() ^ Indent.GetHashCode() ^ JustifyLastLine.GetHashCode() ^
                    ReadingOrder.GetHashCode() ^ RelativeIndent.GetHashCode() ^ ShrinkToFit.GetHashCode() ^
                    TextRotation.GetHashCode() ^ VerticalAlignment.GetHashCode() ^ WrapText.GetHashCode();
        }

        public static Alignment LoadFromXml (XmlNode node)
        {
            return Loader.Load<Alignment>(node); 
        }
    }
}

