/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using DocumentFormat.OpenXml.Model.Worksheets;
using System.Collections.Generic;
using System;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("gradientFill")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class GradientFill
    {
        private double bottomConvergence;
        private double leftConvergence;
        private double rightConvergence;
        private double topConvergence;

        public GradientFill ():
            this(GradientType.Linear, 0)
        {
        }

        public GradientFill (GradientType type, double degree, params Color[] colors):
            this(type, colors)
        {
            LinearGradientDegree = degree;
        }

        public GradientFill (GradientType type, params Color[] colors)
        {
            Type = type;
            foreach (Color color in colors) {
                AddGradientStop(color);
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("stop")]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "stop")]
        public List<GradientStop> Stops {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("degree")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public double LinearGradientDegree {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("bottom")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public double BottomConvergence {
            get {
                return bottomConvergence;
            }
            set {
                Check (value, "Bottom");
                bottomConvergence = value;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("left")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public double LeftConvergence {
            get {
                return leftConvergence;
            }
            set {
                Check (value, "Left");
                leftConvergence = value;
            }
        }
                
        [YAXSerializableField]
        [YAXSerializeAs("right")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public double RightConvergence {
            get {
                return rightConvergence;
            }
            set {
                Check(value, "Right");
                rightConvergence = value;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("top")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public double TopConvergence {
            get {
                return topConvergence;
            }
            set {
                Check(value, "Top");
                topConvergence = value;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("type")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public GradientType Type {
            get;
            set;
        }

        private void Check (double value, string side)
        {
            if (value < 0 || value > 1) {
                string message = String.Format(
                    "{0} convergence of gradient fill is restricted to values ranging from 0 to 1.", side);
                throw new ArgumentOutOfRangeException("value", value, message);
            }
        }

        public void AddGradientStop(Color color)
        {
            if (Stops == null) {
                Stops = new List<GradientStop>();
            }
            GradientStop newStop = new GradientStop(color, Stops.Count);
            Stops.Add(newStop);
        }

        public static bool operator ==(GradientFill a, GradientFill b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return HaveTheSameGradientStops(a.Stops, b.Stops) && a.LinearGradientDegree == b.LinearGradientDegree &&
                a.BottomConvergence == b.BottomConvergence && a.LeftConvergence == b.LeftConvergence &&
                a.RightConvergence == b.RightConvergence && a.TopConvergence == b.TopConvergence && a.Type == b.Type;
        }

        private static bool HaveTheSameGradientStops(List<GradientStop> a, List<GradientStop> b)
        {
            if (a == null && b == null) {
                return true;
            }
            if (a == null || b == null) {
                return false;
            }
            if (a.Count != b.Count) {
                return false;
            }
            for (int i = 0; i < a.Count; i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(GradientFill a, GradientFill b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[GradientFill: Stops={0}, LinearGradientDegree={1}, BottomConvergence={2}, LeftConvergence={3}, RightConvergence={4}, TopConvergence={5}, Type={6}]", Stops, LinearGradientDegree, BottomConvergence, LeftConvergence, RightConvergence, TopConvergence, Type);
        }

        public override bool Equals (object obj)
        {
            if (obj is GradientFill) {
                return ((obj as GradientFill) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Stops.GetHashCode() ^ LinearGradientDegree.GetHashCode() ^ BottomConvergence.GetHashCode() ^
                    LeftConvergence.GetHashCode() ^ RightConvergence.GetHashCode() ^ TopConvergence.GetHashCode() ^
                    Type.GetHashCode();
        }

        public static GradientFill LoadFromXml (XmlNode node)
        {
            return Loader.Load<GradientFill>(node);
        }
    }
}

