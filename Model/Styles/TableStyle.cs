using Logging;
using System.Collections.Generic;
using System.Xml;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("tableStyle")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class TableStyle
    {
        [YAXSerializableField]
        [YAXSerializeAs("tableStyle")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "tableStyleElement")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public List<TableStyleElement> TableStyleElements
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("tableStyle")]
        public int Count
        {
            get
            {
                return TableStyleElements != null ? TableStyleElements.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("name")]
        [YAXAttributeFor("tableStyle")]
        public string Name
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("pivot")]
        [YAXAttributeFor("tableStyle")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = true)]
        public bool PivotStyle
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("table")]
        [YAXAttributeFor("tableStyle")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = true)]
        public bool Table
        {
            get;
            set;
        }

        public static bool operator ==(TableStyle a, TableStyle b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            bool tableStyleElementsEqual = CompareTableStyleElements(a.TableStyleElements, b.TableStyleElements);
            return tableStyleElementsEqual && a.Name == b.Name && a.PivotStyle == b.PivotStyle && a.Table == b.Table;
        }

        private static bool CompareTableStyleElements(List<TableStyleElement> a, List<TableStyleElement> b)
        {
            if (a.Count != b.Count) {
                return false;
            }
            for (int i = 0; i < a.Count; i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(TableStyle a, TableStyle b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return string.Format("[TableStyle: TableStyleElements={0}, Count={1}, Name={2}, PivotStyle={3}, Table={4}]", TableStyleElements, Count, Name, PivotStyle, Table);
        }

        public override bool Equals(object obj)
        {
            if (obj is TableStyle) {
                return ((obj as TableStyle) == this);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return TableStyleElements.GetHashCode() ^ Name.GetHashCode() ^ PivotStyle.GetHashCode() ^ Table.GetHashCode();
        }

        public static TableStyle LoadFromXml(XmlNode node)
        {
            Logger.Log("TODO: add loading table styles from xml");
            return new TableStyle();
        }
    }
}

