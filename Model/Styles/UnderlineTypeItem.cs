using DocumentFormat.OpenXml.Enums;
using DocumentFormat.OpenXml.XmlHandling;
using Logging;
using System.Xml;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class UnderlineTypeItem
    {
        public UnderlineTypeItem() :
            this(UnderlineType.SingleUnderline)
        {
        }

        public UnderlineTypeItem(UnderlineType value)
        {
            Value = value;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("val")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public UnderlineType Value
        {
            get;
            set;
        }

        public static bool operator ==(UnderlineTypeItem a, UnderlineTypeItem b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Value == b.Value;
        }

        public static bool operator !=(UnderlineTypeItem a, UnderlineTypeItem b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return string.Format("[CharacterStyleItem: Value={0}]", Value);
        }

        public override bool Equals(object obj)
        {
            if (obj is UnderlineTypeItem) {
                return ((obj as UnderlineTypeItem) == this);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static UnderlineTypeItem LoadFromXml(XmlNode node)
        {
            UnderlineTypeItem item = Loader.Load<UnderlineTypeItem>(node);
            if (item == null) {
                if (node.LocalName == "u") {
                    item = new UnderlineTypeItem(UnderlineType.SingleUnderline);
                } else {
                    Logger.Warning("OOXML Loader: No underline style item loaded for node '{0}'", node.LocalName);
                }
            }
            return item;
        }
    }
}

