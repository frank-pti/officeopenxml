/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("fill")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
	public class Fill
	{
        GradientFill gradientFill;
        PatternFill patternFill;

        public Fill ()
        {
        }

        public Fill (GradientFill gradientFill)
        {
            this.gradientFill = gradientFill;
            patternFill = null;
        }

        public Fill (PatternFill patternFill)
        {
            this.patternFill = patternFill;
            gradientFill = null;
        }

        [YAXSerializableField]
        [YAXSerializeAs ("gradientFill")]
        public GradientFill GradientFill {
            get {
                return gradientFill;
            }
            set {
                gradientFill = value;
                if (gradientFill != null) {
                    patternFill = null;
                }
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs ("patternFill")]
        public PatternFill PatternFill {
            get {
                return patternFill;
            }
            set {
                patternFill = value;
                if (patternFill != null) {
                    gradientFill = null;
                }
            }
        }

        public static bool operator ==(Fill a, Fill b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.GradientFill == b.GradientFill && a.PatternFill == b.PatternFill;
        }

        public static bool operator !=(Fill a, Fill b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Fill: GradientFill={0}, PatternFill={1}]", GradientFill, PatternFill);
        }

        public override bool Equals (object obj)
        {
            if (obj is Fill) {
                return ((obj as Fill) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return GradientFill.GetHashCode() ^ PatternFill.GetHashCode();
        }

        public static Fill LoadFromXml (XmlNode node)
        {
            return Loader.Load<Fill>(node);
        }
	}
}

