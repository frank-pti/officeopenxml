/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using DocumentFormat.OpenXml.Model.Worksheets;
using System;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("patternFill")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class PatternFill
    {
        public PatternFill():
            this(PatternType.None)
        {
        }

        public PatternFill (PatternType patternType)
        {
            PatternType = patternType;
        }

        [YAXSerializableField]
        [YAXSerializeAs("patternType")]
        [YAXAttributeForClass]
        public PatternType PatternType {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("fgColor")]
        public Color Foreground {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("bgColor")]
        public Color Background {
            get;
            set;
        }

        public static bool operator ==(PatternFill a, PatternFill b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.PatternType == b.PatternType && a.Foreground == b.Foreground && a.Background == b.Background;
        }

        public static bool operator !=(PatternFill a, PatternFill b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[PatternFill: PatternType={0}, Foreground={1}, Background={2}]", PatternType, Foreground, Background);
        }

        public override bool Equals (object obj)
        {
            if (obj is PatternFill) {
                return ((obj as PatternFill) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return PatternType.GetHashCode() ^ Foreground.GetHashCode() ^ Background.GetHashCode();
        }

        public static PatternFill LoadFromXml (XmlNode node)
        {
            return Loader.Load<PatternFill>(node);
        }
    }
}

