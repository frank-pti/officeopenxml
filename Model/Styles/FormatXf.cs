/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using System.Xml;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("xf")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class FormatXf: FormattingRecordsXf
    {
        public FormatXf(params XfApplyTypes[] applyTypes):
            base(applyTypes)
        {
        }

        public FormatXf(FormattingRecordsXf xf, uint formatId):
            base(xf)
        {
            FormatId = formatId;
        }

        [YAXSerializableField]
        [YAXSerializeAs("xfId")]
        [YAXAttributeForClass]
        public Nullable<uint> FormatId {
            get;
            set;
        }

        public static bool operator == (FormatXf a, FormatXf b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return ((FormattingRecordsXf)a == (FormattingRecordsXf)b) && (a.FormatId == b.FormatId);
        }

        public static bool operator != (FormatXf a, FormatXf b)
        {
            return !(a == b);
        }

        public override bool Equals (object obj)
        {
            if (obj is FormatXf) {
                return (((FormatXf) obj) == this);
            }
            return false;
        }

        public override string ToString ()
        {
            return string.Format ("[FormatXf: base={0}, FormatId={1}]", base.ToString(), FormatId);
        }

        public override int GetHashCode ()
        {
            return base.GetHashCode () ^ FormatId.GetHashCode();
        }

        public static new FormatXf LoadFromXml(XmlNode node)
        {
            uint xfId = uint.Parse(node.Attributes["xfId"].Value);
            return new FormatXf(FormattingRecordsXf.LoadFromXml(node), xfId);
        }
    }
}

