/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model.Worksheets;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("colors")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Colors
    {
        public Colors()
        {
            IndexedColors = CreateDefaultColorIndex();
        }

        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "color")]
        [YAXSerializeAs("mruColors")]
        public List<Color> MruColors {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "rgbColor")]
        [YAXSerializeAs("indexedColors")]
        public List<IndexColor> IndexedColors {
            get;
            set;
        }

        private static List<IndexColor> CreateDefaultColorIndex ()
        {
            return new List<IndexColor>() {
                new IndexColor(0x00000000), new IndexColor(0x00FFFFFF), new IndexColor(0x00FF0000),   // 0, 1, 2
                new IndexColor(0x0000FF00), new IndexColor(0x000000FF), new IndexColor(0x00FFFF00),   // 3, 4, 5
                new IndexColor(0x00FF00FF), new IndexColor(0x0000FFFF), new IndexColor(0x00800000),   // 6, 7, 8
                new IndexColor(0x00008000), new IndexColor(0x00000080), new IndexColor(0x00808000),   // 9,10,11
                new IndexColor(0x00800080), new IndexColor(0x00008080), new IndexColor(0x00CCCCCC),   //12,13,14
                new IndexColor(0x00808080), new IndexColor(0x009999FF), new IndexColor(0x00993366),   //15,16,17
                new IndexColor(0x00FFFFCC), new IndexColor(0x00CCFFFF), new IndexColor(0x00660066),   //18,19,20
                new IndexColor(0x00FF8080), new IndexColor(0x000066CC), new IndexColor(0x00CCCCFF),   //21,22,23
                new IndexColor(0x00000080), new IndexColor(0x00FF00FF), new IndexColor(0x00FFFF00),   //24,25,26
                new IndexColor(0x0000FFFF), new IndexColor(0x00800080), new IndexColor(0x00800000),   //27,28,29
                new IndexColor(0x00008080), new IndexColor(0x000000FF), new IndexColor(0x0000CCFF),   //30,31,32
                new IndexColor(0x00CCFFFF), new IndexColor(0x00CCFFCC), new IndexColor(0x00FFFF99),   //33,34,35
                new IndexColor(0x0099CCFF), new IndexColor(0x00FF99CC), new IndexColor(0x00CC99FF),   //36,37,38
                new IndexColor(0x00FFCC99), new IndexColor(0x003366FF), new IndexColor(0x0033CCCC),   //39,40,41
                new IndexColor(0x0099CC00), new IndexColor(0x00FFCC00), new IndexColor(0x00FF9900),   //42,43,44
                new IndexColor(0x00FF6600), new IndexColor(0x00666699), new IndexColor(0x00969696),   //45,46,47
                new IndexColor(0x00003366), new IndexColor(0x00339966), new IndexColor(0x00003300),   //48,49,50
                new IndexColor(0x00333300), new IndexColor(0x00993300), new IndexColor(0x00993366),   //51,52,53
                new IndexColor(0x00333399), new IndexColor(0x00333333)                                  //54,55
            };
        }

        public static bool operator == (Colors a, Colors b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return CheckColors<Color>(a.MruColors, b.MruColors) &&
                CheckColors<IndexColor>(a.IndexedColors, b.IndexedColors);
        }

        private static bool CheckColors<T>(List<T> a, List<T> b)
            where T: class
        {
            if (a == null && b == null) {
                return true;
            }
            if (a == null || b == null) {
                return false;
            }
            if (a.Count != b.Count) {
                return false;
            }
            for (int i = 0; i < a.Count; i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }

        public static bool operator != (Colors a, Colors b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Colors: MruColors={0}, IndexedColors={1}]", MruColors, IndexedColors);
        }

        public override bool Equals (object obj)
        {
            if (obj is Colors) {
                return ((obj as Colors) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return MruColors.GetHashCode() ^ IndexedColors.GetHashCode();
        }
    }
}

