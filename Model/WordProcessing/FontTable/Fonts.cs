/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model.WordProcessing.FontTable
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("fonts")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
    public class Fonts: PackageXmlSerializable
    {
        public Fonts()
        {
            FontList = new List<Font>();
        }

        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "font")]
        public List<Font> FontList
        {
            get;
            set;
        }

        public void AddFont(Font font)
        {
            FontList.Add(font);
        }

        #region implemented abstract members of DocumentFormat.OpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.WordProcessingFontTableXml;
            }
        }
        #endregion

    }
}

