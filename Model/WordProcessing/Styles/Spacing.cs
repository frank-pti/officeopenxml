/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("style")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class Spacing
	{
        private const double InchToTwips = 1440.0;
        private const double MmToTwips = InchToTwips/25.4;

        [YAXSerializableField]
        [YAXSerializeAs("after")]
        [YAXAttributeForClass]
        public int AfterTwips {
            get;
            set;
        }

        public void SetAfter(double value, Unit unit)
        {
            AfterTwips = (int)(value * CalculateFactor(unit));
        }

        [YAXSerializableField]
        [YAXSerializeAs("afterAutospacing")]
        [YAXAttributeForClass]
        public bool AfterAutoSpacing {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("before")]
        [YAXAttributeForClass]
        public int BeforeTwips {
            get;
            set;
        }

        public void SetBefore(double value, Unit unit)
        {
            BeforeTwips = (int)(value * CalculateFactor(unit));
        }

        [YAXSerializableField]
        [YAXSerializeAs("beforeAutospacing")]
        [YAXAttributeForClass]
        public bool BeforeAutoSpacing {
            get;
            set;
        }

        private double CalculateFactor(Unit unit)
        {
            switch (unit) {
            case Unit.Millimeter:
                return MmToTwips;
            case Unit.Inch:
                return InchToTwips;
            default:
                return 1.0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("line")]
        [YAXAttributeForClass]
        public int Line {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("lineRule")]
        [YAXAttributeForClass]
        public LineRule LineRule {
            get;
            set;
        }

        public double LineSpacing {
            get {
                return (double)Line/240.0;
            }
            set {
                Line = (int)(value * 240.0);
                LineRule = LineRule.Auto;
            }
        }
	}
}

