/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using System.Globalization;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("rFonts")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class RunFonts
	{
        public RunFonts()
        {
            Ascii = "Times New Roman";
            ComplexScript = "Lohit Hindi";
            EastAsia = "DejaVu Sans";
            HAnsi = Ascii;
        }

        [YAXSerializableField]
        [YAXSerializeAs("ascii")]
        [YAXAttributeForClass]
        public string Ascii {
            get;
            set;
        }
                
        [YAXSerializableField]
        [YAXSerializeAs("cs")]
        [YAXAttributeForClass]
        public string ComplexScript {
            get;
            set;
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("eastAsia")]
        [YAXAttributeForClass]
        public string EastAsia {
            get;
            set;
        }
        
        [YAXSerializableField]
        [YAXSerializeAs("hAnsi")]
        [YAXAttributeForClass]
        public string HAnsi {
            get;
            set;
        }
	}
}

