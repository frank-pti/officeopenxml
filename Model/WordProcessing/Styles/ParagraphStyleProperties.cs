/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("style")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class ParagraphStyleProperties
	{
        public bool KeepNext {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("widowControl")]
        public bool WidowControl {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("suppressAutoHyphens")]
        public bool SuppressAutoHypens {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("keepNext")]
        public object KeepNextForExport {
            get {
                return KeepNext ? new object() {} : null;
            } set {
                KeepNext = (value != null);
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("spacing")]
        public Spacing Spacing {
            get;
            set;
        }
	}
}

