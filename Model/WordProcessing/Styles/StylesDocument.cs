/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("styles")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
    public class StylesDocument: PackageXmlSerializable
    {
        public StylesDocument ()
        {
            Style = new List<IStyle>();
        }

        [YAXSerializableField]
        [YAXSerializeAs("style")]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "style")]
        public List<IStyle> Style {
            get;
            set;
        }

        public void AddStyle(IStyle style)
        {
            this.Style.Add(style);
        }

        #region implemented abstract members of DocumentFormat.OpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.WordProcessingStylesXml;
            }
        }
        #endregion

        public IStyle DefaultStyle {
            get {
                if (Style.Count < 1) {
                    return null;
                }
                int i = 0;
                IStyle style = Style[i];
                while (!style.Default && i < Style.Count) {
                    i++;
                    style = Style[i];
                }
                return style.Default ? style : null;
            }
        }
    }
}

