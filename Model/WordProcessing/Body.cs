/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model.WordProcessing.Paragraphs;

namespace DocumentFormat.OpenXml.Model.WordProcessing
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("body")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class Body
	{
        public Body()
        {
            Paragraphs = new List<Paragraph>();
        }

        [YAXSerializableField]
        [YAXSerializeAs("p")]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "p")]
        public List<Paragraph> Paragraphs {
            get;
            set;
        }

        public void AddParagraph(Paragraph paragraph)
        {
            Paragraphs.Add(paragraph);
        }

        public void AddParagraph(string text)
        {
            Paragraphs.Add(new Paragraph(text));
        }

        public void AddParagraph(string text, ParagraphProperties properties)
        {
            Paragraphs.Add(new Paragraph(text, properties));
        }
	}
}

