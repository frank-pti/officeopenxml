/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.WordProcessing.RunContent
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("br")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
    public class Break: ITextRunContent
    {
        public Break(): this(BreakType.LineBreak)
        {
        }

        public Break (BreakType breakType)
        {
            BreakType = breakType;
        }

        [YAXSerializableField]
        [YAXSerializeAs("type")]
        public BreakType BreakType {
            get;
            set;
        }

        public static Break ColumnBreak {
            get {
                return new Break(BreakType.ColumnBreak);
            }
        }

        public static Break PageBreak {
            get {
                return new Break(BreakType.PageBreak);
            }
        }

        public static Break LineBreak {
            get {
                return new Break(BreakType.LineBreak);
            }
        }
    }
}

