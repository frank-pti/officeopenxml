/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model.WordProcessing.RunContent;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Run
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("r")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class TextRun
	{
        public static readonly String[] DefaultSplitString = new string[] { "<br/>", "<br />", "\r", "\n" };

        public TextRun()
        {
            Text = new List<ITextRunContent>();
        }

        public TextRun(string text):
            this(text, DefaultSplitString)
        {
        }

        public TextRun(string text, string[] splitText, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries):
            this(text.Split(splitText, splitOptions))
        {
        }

        public TextRun(string[] lines): this()
        {
            foreach (string line in lines) {
                Text.Add(new Text(line));
                if (line != lines[lines.Length - 1]) {
                    Text.Add(Break.LineBreak);
                }
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("t")]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement)]
        public List<ITextRunContent> Text {
            get;
            set;
        }
	}
}

