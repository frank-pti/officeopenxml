/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Settings
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("settings")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
    public class DocumentSettings: PackageXmlSerializable
    {
        public DocumentSettings ()
        {
            Zoom = 100;
            DefaultTabStop = 709;
        }

        [YAXSerializableField]
        [YAXAttributeFor("zoom")]
        [YAXSerializeAs("percent")]
        public int Zoom {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeFor("defaultTabStop")]
        [YAXSerializeAs("val")]
        public int DefaultTabStop {
            get;
            set;
        }

        #region implemented abstract members of DocumentFormat.OpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.WordProcessingSettingsXml;
            }
        }
        #endregion

    }
}

