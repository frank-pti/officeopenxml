/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model.CalculationChain
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("calcChain")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class CalcChain: PackageXmlSerializable, IPart
    {
        internal const string DefaultDocumentPath = "/xl/calcChain.xml";

        private List<CalcChainCell> cells;

        public CalcChain()
        {
            InsertPosition = 0;
        }

        [YAXSerializableField]
        [YAXCollection (YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "c")]
        public List<CalcChainCell> Cells {
            get {
                return cells;
            }
            set {
                cells = value;
                int previousSheetId = 0;
                foreach (CalcChainCell cell in cells) {
                    if (cell.SheetId == 0) {
                        cell.SheetId = previousSheetId;
                    } else {
                        previousSheetId = cell.SheetId;
                    }
                }
            }
        }

        public void AddCell(uint rowNum, uint colNum, string documentPath)
        {
            if (Cells == null) {
                Cells = new List<CalcChainCell>();
            }
            CalcChainCell cell = new CalcChainCell(rowNum, colNum, documentPath);
            Cells.Insert(InsertPosition, cell);
        }

        public int InsertPosition {
            get;
            set;
        }

        #region implemented abstract members of DocumentFormat.OpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.CalcChainXml;
            }
        }
        #endregion

        #region IStreamSerializable implementation
        public void Serialize (System.IO.Stream stream)
        {
            SerializeStreamXml(stream);
        }
        #endregion

        #region IPart implementation
        public string DocumentPath {
            get {
                return DefaultDocumentPath;
            }
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipCalcChain;
            }
        }
        #endregion
    }
}

