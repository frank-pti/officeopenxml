/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.CalculationChain
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("c")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class CalcChainCell: CellBase
    {
        [Obsolete("use for yaxlib deserialization only")]
        public CalcChainCell():
            base(0, 0)
        {
        }

        public CalcChainCell(uint rowNum, uint colNum, string sheetDocumentPath):
            base(rowNum, colNum)
        {
            SheetDocumentPath = sheetDocumentPath;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("a")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public bool Array {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("i")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = 0)]
        public int SheetId {
            get;
            set;
        }

        public string SheetDocumentPath {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("l")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public bool NewDependecyLevel {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("s")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public bool ChildChain {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("t")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = false)]
        public bool NewThread {
            get;
            set;
        }
    }
}

