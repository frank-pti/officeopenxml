/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model.SharedStringsTable;
using DocumentFormat.OpenXml.Model.Styles;
using DocumentFormat.OpenXml.Enums;
using System.Reflection;
using System.ComponentModel;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("pageSetup")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
	public class PageSetup
	{
        private PaperSize paperSize;

        public PageSetup()
        {
            Orientation = Orientation.Default;
            PaperSize = PaperSize.A4;
        }

        [YAXSerializableField]
        [YAXSerializeAs("orientation")]
        [YAXAttributeForClass]
        public Orientation Orientation {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("paperSize")]
        [YAXAttributeForClass]
        public int PaperSizeNumber {
            get {
                return (int)PaperSize;
            }
            set {
                PaperSize = (PaperSize)Enum.ToObject(typeof(PaperSize), value);
            }
        }

        public PaperSize PaperSize {
            get {
                return paperSize;
            }
            set {
                paperSize = value;
            }
        }
    }
}
