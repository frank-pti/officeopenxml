using DocumentFormat.OpenXml.XmlHandling;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Xml;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("color")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Color
    {
        private Nullable<uint> argbData;
        private Nullable<double> tint;

        public Color()
        {
        }

        public Color(uint argb)
        {
            Argb = argb;
        }

        public Color(string argb)
        {
            Rgb = argb;
        }

        [YAXSerializableField]
        [YAXSerializeAs("auto")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<bool> Automatic
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("rgb")]
        [YAXAttributeForClass]
        public string Rgb
        {
            get
            {
                if (Argb == null) {
                    return null;
                }
                return String.Format("{0:X08}", Argb);
            }
            set
            {
                if (String.IsNullOrWhiteSpace(value)) {
                    Argb = null;
                } else {
                    Argb = uint.Parse(value, System.Globalization.NumberStyles.AllowHexSpecifier);
                }
            }
        }

        public Nullable<uint> Argb
        {
            get
            {
                return argbData;
            }
            set
            {
                argbData = value;
                if (argbData != null) {
                    argbData = new Nullable<uint>(argbData.Value & 0xFFFFFFFF);
                }
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("theme")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> Theme
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("tint")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<double> Tint
        {
            get
            {
                return tint;
            }
            set
            {
                if (value < -1.0 || value > 1.0) {
                    throw new ArgumentOutOfRangeException("value", value, "The tint value is stoared as double from -1.0 to 1.0.");
                }
                tint = value;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("indexed")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<uint> Index
        {
            get;
            set;
        }

        public static bool operator ==(Color a, Color b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return (a.Argb == b.Argb) &&
                (a.Automatic == b.Automatic) &&
                (a.Theme == b.Theme) &&
                (a.Tint == b.Tint) &&
                (a.Index == b.Index);
        }

        public static bool operator !=(Color a, Color b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is Color) {
                return ((Color)obj == this);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Color LoadFromXml(XmlNode node)
        {
            return Loader.Load<Color>(node);
        }
    }
}

