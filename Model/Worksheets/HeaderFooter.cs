/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("headerFooter")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class HeaderFooter
    {
        private const string HeaderFooterFormat = "{1}{0}{2}{3}{0}{4}{5}{0}{6}";

        [YAXSerializableField]
        [YAXSerializeAs("oddHeader")]
        public string OddHeaderStringValue {
            get {
                return String.Format(HeaderFooterFormat, HeaderFooterCode.RegularFontStyle,
                    HeaderFooterCode.LeftSection, OddHeader.Left, 
                    HeaderFooterCode.CenterSection, OddHeader.Center,
                    HeaderFooterCode.RightSection, OddHeader.Right);
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("oddFooter")]
        public string OddFooterValue {
            get {
                return String.Format(HeaderFooterFormat, HeaderFooterCode.RegularFontStyle,
                    HeaderFooterCode.LeftSection, OddFooter.Left, 
                    HeaderFooterCode.CenterSection, OddFooter.Center,
                    HeaderFooterCode.RightSection, OddFooter.Right);
            }
        }

        public HeaderFooterItem OddHeader {
            get;
            set;
        }

        public HeaderFooterItem OddFooter {
            get;
            set;
        }

        public static HeaderFooter CreateDefault()
        {
            return new HeaderFooter() {
                OddHeader = new HeaderFooterItem() {
                    Right = String.Format("{0} {1}", HeaderFooterCode.Date, HeaderFooterCode.Time)
                },
                OddFooter = new HeaderFooterItem() {
                    Left = String.Format("{0} - {1}", HeaderFooterCode.WorkbookFileName, HeaderFooterCode.SheetTabName),
                    Right = String.Format("Page {0} of {1}", HeaderFooterCode.CurrentPage, HeaderFooterCode.TotalPages)
                }
            };
        }
    }

}

