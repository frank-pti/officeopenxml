/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("col")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Column
    {
        //TODO: to be calculated
        private const double MaximumDigitWidth = 7;

        [YAXSerializableField]
        [YAXSerializeAs("max")]
        [YAXAttributeForClass]
        public uint Maximum {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("min")]
        [YAXAttributeForClass]
        public uint Minimum {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("width")]
        [YAXAttributeForClass]
        public double Width {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("style")]
        [YAXAttributeForClass]
        public uint Style {
            get;
            set;
        }

        public static double CalculateWidth(int stringLength)
        {
            return Math.Truncate((((stringLength * MaximumDigitWidth + 5.0)/ MaximumDigitWidth) * 256.0) / 256.0);
        }

        public static bool operator == (Column a, Column b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return (a.Maximum == b.Maximum) && (a.Minimum == b.Minimum) && (a.Width == b.Width) && (a.Style == b.Style);
        }

        public static bool operator != (Column a, Column b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Column: Maximum={0}, Minimum={1}, Width={2}, Style={3}]", Maximum, Minimum, Width, Style);
        }

        public override bool Equals (object obj)
        {
            if (obj is Column) {
                return ((Column)obj == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Maximum.GetHashCode() ^ Minimum.GetHashCode() ^ Width.GetHashCode() ^ Style.GetHashCode();
        }
    }
}
