/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("c")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class NumberCell: Cell
    {
        public NumberCell(double value, uint rowNum, uint colNum):
            this(value, 0, rowNum, colNum)
        {
        }

        public NumberCell(double value, uint style, uint rowNum, uint colNum):
            this(value, null, style, rowNum, colNum)
        {
        }

        public NumberCell (double value, string formula, uint rowNum, uint colNum):
            this(value, formula, 0, rowNum, colNum)
        {
        }

        public NumberCell (double value, string formula, uint style, uint rowNum, uint colNum):
            base(CellType.Number, rowNum, colNum)
        {
            this.Formula = formula;
            this.Value = value;
            this.Style = style;
        }

        [YAXSerializableField]
        [YAXSerializeAs("f")]
        public string Formula {
            get;
            set;
        }
    }
}

