/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model.ChartDrawing
{
    public class DataCache<T>
    {
        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("ptCount")]
        public int PointCount {
            get {
                return Points.Count;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("pt")]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "pt")]
        public List<DataPoint<T>> Points {
            get;
            set;
        }

        public void AddPoint(T value)
        {
            if (Points == null) {
                Points = new List<DataPoint<T>>();
            }
            int index = Points.Count;
            Points.Add(new DataPoint<T>(index, value));
        }
    }
}

