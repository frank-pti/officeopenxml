/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.ChartDrawing
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("tx")]
    [YAXNamespace("c", XmlNamespaces.OfficeOpenDrawingChart)]
	public class ChartText
	{
        public ChartText(string text, double sizeInPoints)
        {
            RichText = new RichText(text, sizeInPoints);
        }

        [YAXSerializableField]
        [YAXSerializeAs("strRef")]
        public StringReference StringReference {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("rich")]
        public RichText RichText {
            get;
            set;
        }
	}

}

