/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.ChartDrawing
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("plotArea")]
    [YAXNamespace("c", XmlNamespaces.OfficeOpenDrawingChart)]
    public class PlotArea
    {
        [YAXSerializableField]
        [YAXSerializeAs("layout")]
        public Layout Layout {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("scatterChart")]
        public ScatterChart Chart {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("valAx")]
        public ValueAxis XAxis {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("valAx")]
        public ValueAxis YAxis {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("spPr")]
        public ShapeProperties ShapeProperties {
            get;
            set;
        }
    }
}

