/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace DocumentFormat.OpenXml
{
	public static class ContentTypes
	{
		public const string RelationshipsXml = "application/vnd.openxmlformats-package.relationships+xml";
		public const string WorksheetXml = "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml";
		public const string WorkbookXml = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml";
		public const string SharedStringsXml = "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml";
		public const string StylesXml = "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml";
		public const string ExtendedPropertiesXml = "application/vnd.openxmlformats-officedocument.extended-properties+xml";
		public const string CorePropertiesXml = "application/vnd.openxmlformats-package.core-properties+xml";
        public const string CalcChainXml = "application/vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml";
        public const string DrawingXml = "application/vnd.openxmlformats-officedocument.drawing+xml";
        public const string ChartXml = "application/vnd.openxmlformats-officedocument.drawingml.chart+xml";
        public const string WordProcessingXml = "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml";
        public const string WordProcessingStylesXml = "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml";
        public const string WordProcessingSettingsXml = "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml";
        public const string WordProcessingFontTableXml = "application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml";
        public const string CustomXml = "application/vnd.openxmlformats-officedocument.customXmlProperties+xml";
	}
}

