/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;

namespace DocumentFormat.OpenXml
{
    public class CopyPart: IPart
    {
        private Stream source;

        public CopyPart (string contentType, string documentPath, string relId, string nameSpace, Stream source)
        {
            this.source = source;
            ContentType = contentType;
            DocumentPath = documentPath;
            Namespace = nameSpace;
            RelId = relId;
        }

        #region IPart implementation
        public void Serialize (Stream stream)
        {
            source.CopyTo(stream);
        }

        public string ContentType {
            get;
            private set;
        }

        public string DocumentPath {
            get;
            private set;
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get;
            private set;
        }
        #endregion
    }
}

